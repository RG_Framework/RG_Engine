Our query engine, called RG Engine, is built for processing RG-SQL queries that
contain graph sub-queries (queries with graph operators) and relational 
sub-queries. The RG engine is developed in Python programming language with 
the official PostgreSQL client library – libpq. We use Psycopg, 
the current mature wrapper for the libpq, as the PostgreSQL adapter 
for our query engine.
<br>
<br>
You can start the query engine by running the queryConsole program.
<br>
<br>
More details about RG-SQL and the RG Engine, please refer to 
the thesis "Towards a Unified Framework for Network Analytics" 
(the link is to be published).