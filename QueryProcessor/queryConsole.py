'''
The queryConsole is to read the query input, show query results and display error information

@author: minjian
'''

import psycopg2
import queryParser
import matGraphProcessor
import time
import os

#starts to execute the input query
def execQuery(conn, cur, executeCommand):
    lowerCaseCommand = executeCommand.lower()
    
    #graph query contains rank, cluster and path operation
    if ("rank" in lowerCaseCommand) or ("cluster" in lowerCaseCommand)or ("path" in lowerCaseCommand):
        startTime = time.time()
        
        newExecuteCommand = queryParser.queryAnalyse(executeCommand, conn, cur)
        #newExecuteCommand = graphProcessor.queryAnalyse(executeCommand, conn, cur)
        #print "Total operation time is: ", (time.time() - startTime)
        print newExecuteCommand  #for debug
        cur.execute(newExecuteCommand[1:]) #remove the first space
        printResult(conn, cur)
    
    #query about creating or dropping a materialised graph    
    elif ("create" in lowerCaseCommand or "drop" in lowerCaseCommand) and ("ungraph" in lowerCaseCommand or "digraph" in lowerCaseCommand):
        newExecuteCommand = matGraphProcessor.processCommand(executeCommand, conn, cur)
        eIndex = newExecuteCommand.index("view")
        cur.execute(newExecuteCommand[1:]) #remove the first space
        conn.commit()
        print newExecuteCommand[1:eIndex] + "graph"
    
    #normal relational query without any graph functions
    else:
        print executeCommand[1:]
        cur.execute(executeCommand[1:])  #remove the first space
        printResult(conn, cur)

#prints results received from the database
def printResult(conn, cur):
    rows = cur.fetchall()
    for i in rows:
        for each in i:
            print str(each) + '\t',
        print
    conn.commit() 



#starts the main function   
homeDir = os.environ['HOME']
memDir = "/dev/shm"

if os.path.exists(homeDir + "/RG_Mat_Graph") == False:
    os.mkdir(homeDir + "/RG_Mat_Graph")
    
if os.path.exists(memDir + "/RG_Tmp_Graph") == False:
    os.mkdir(memDir + "/RG_Tmp_Graph")    

while True:
    query = []
    
    #Here is connect to your PostgreSQL
    #Change you database, user and port here
    db = "acm"
    dbUser = "minjian"
    dbPort = 5433
    
    conn = psycopg2.connect(database=db, user=dbUser, port=dbPort)
    cur = conn.cursor()
    
    while True:
        try:
            queryLine = raw_input()
            query.append(queryLine)
            if ";" in queryLine:
                break
        except EOFError as reason:
            print "Console terminated"
            exit()
    
    executeCommand = ""
    for each in query:
        executeCommand += (" " + each)
    start_time = time.time()
    try:
        execQuery(conn, cur, executeCommand)
        print "Total query time is: ", (time.time() - start_time)
        os.system("rm -fr /dev/shm/RG_Tmp_Graph/*")  #clear graphs on-the-fly
        queryParser.graphQueryAndResult.clear()  #clear parser's dictionary for result table names and graph sub-queries
    except psycopg2.ProgrammingError as reason:
        print str(reason)
    finally:
        cur.close()
        conn.close()
